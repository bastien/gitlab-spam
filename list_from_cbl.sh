# THIS FILE LISTS ACCOUNTS CREATED WITH IP BLACKLISTED IN CBL. USE THE
# --ask OPTION TO ASK BEFORE DELETION
#
# API ENDPOINT GOES TO api.txt: echo -n "https://my-gitlab-instance/api/v4" > api.txt
# TOKEN GOES TO token.txt: echo -n my_admin_token > token.txt

set -e

token=$(cat token.txt)
api=$(cat api.txt)

wl_emails=$(cat wl_emails.txt) # A list of blacklisted countries
wl_users=$(cat wl_users.txt) # A list of whitelisted user IDs

total_pages=$(curl --silent --head --header "PRIVATE-TOKEN: $token" "$api/users?order_by=id&sort=desc&per_page=100&without_projects=true" | awk -F':[ \t]+' 'tolower($1) ~ /^x-total-pages$/ { print toupper($2) }' | sed "s/\r//g")

banned_ips=""

echo -n "Getting list of users..."
for ((page=1; page<=$total_pages; page++))
do
	ids="$ids $(curl --silent --request GET --header "PRIVATE-TOKEN: $token" "$api/users?per_page=100&page=$page" | jq -r '.[].id')"
done

echo " done!"

for id in $ids
do
	api_output=$(curl --silent --request GET --header "PRIVATE-TOKEN: $token" "$api/users/$id")

	username=$(echo $api_output | jq -r '.username')
	email=$(echo $api_output | jq -r '.email')
	bio=$(echo $api_output | jq -r '.bio')
	website_url=$(echo $api_output | jq -r '.website_url')
	created_at=$(echo $api_output | jq -r '.created_at')
	confirmed_at=$(echo $api_output | jq -r '.confirmed_at')
	last_activity_on=$(echo $api_output | jq -r '.last_activity_on')
	last_sign_in_at=$(echo $api_output | jq -r '.last_sign_in_at')
	memberships_count=$(( $(curl --silent --request GET --header "PRIVATE-TOKEN: $token" "$api/users/$id/memberships" | jq -r '. | length') ))
	is_admin=$(echo $api_output | jq -r '.is_admin')
	current_sign_in_ip=$(echo $api_output | jq -r '.current_sign_in_ip')

	mark_to_delete="false"

	valid_ipv4="false"
	ipcalc "$current_sign_in_ip" | awk 'BEGIN{FS=":"; is_invalid=0} /^INVALID/ {is_invalid=1} END{exit is_invalid}' && valid_ipv4="true" || valid_ipv4="false"

	listed_in_cbl="No"
	if [ "$valid_ipv4" == "true" ]; then
		arpa=$(echo "$current_sign_in_ip" | awk -F . '{print $4"."$3"."$2"."$1}')
		for return_code in $(dig ${arpa}.zen.spamhaus.org +short)
		do
			if [ "$return_code" == "127.0.0.4" ]; then
				listed_in_cbl="Yes"
				mark_to_delete="true"
			fi
		done
	fi

	if [ "$last_sign_in_at" == "null" ]; then
		last_sign_in_at="never"
	fi

	if [ "$last_activity_on" == "null" ]; then
		last_activity_on="never"
	fi

	for wl_email in $wl_emails
	do
		if [[ "$email" == *"@$wl_email"* ]]; then
			mark_to_delete="false"
			email="$email (whitelisted)"
		fi
	done

	for wl_user in $wl_users
	do
		if [[ $id == "$wl_user" ]]; then
			mark_to_delete="false"
			username="$username (whitelisted)"
		fi
	done

	if [ "$is_admin" == "true" ]; then
		mark_to_delete="false"
		is_admin="Yes"
	else
		is_admin="No"
	fi

	if [ $memberships_count -gt 0 ]; then
		mark_to_delete="false"
	fi


	if [ "$mark_to_delete" == "true" ]; then
		echo "($id) $username"
		echo "Email: $email"
		echo "Created at: $created_at ($confirmed_at)"
		echo "Bio: $bio"
		echo "Website: $website_url"
		echo "Last sign in: $last_sign_in_at"
		echo "Last activity: $last_activity_on"
		echo "Number of projects or groups: $memberships_count"
		echo "Sign in IP: $current_sign_in_ip"
		echo "Is admin: $is_admin"
		if [ "$valid_ipv4" == "true" ]; then
			echo "Listed in CBL: $listed_in_cbl"
		fi

		if [ "$1" == "--ask" ]; then
			duplicate="false"
			for ip in $banned_ips
			do
				if [ "$current_sign_in_ip" == $ip ]; then
					curl --silent --request DELETE --header "PRIVATE-TOKEN: $token" "$api/users/$id?hard_delete=true"
					echo "DELETED!"
					duplicate="true"
				fi
			done

			if [ "$duplicate" == "false" ]; then
				read -p "Delete user? [(y)/n] " delete
				if [ "$delete" == "y" ] || [ "$delete" == "" ]; then
					curl --silent --request DELETE --header "PRIVATE-TOKEN: $token" "$api/users/$id?hard_delete=true"
					echo "DELETED!"
					banned_ips="$current_sign_in_ip $banned_ips"
				fi
			fi
		fi

		echo ""
	fi
done