# THIS FILE LISTS USERS THAT HAS BEEN NEVER ACTIVATED, OR ACTIVATED IN LESS
# THAN 10 SECONDS, OR SIGNED WITH AN IP FROM A BLOCKED COUNTRY, AND NOT IN
# ANY GROUP OR PROJECT. USE THE --delete OPTION TO DELETE THEM. USE
# THE --ask OPTION TO ASK FOR A CONFIRMATION BEFORE DELETION.
#
# API ENDPOINT GOES TO api.txt: echo -n "https://my-gitlab-instance/api/v4" > api.txt
# TOKEN GOES TO token.txt: echo -n my_admin_token > token.txt

set -e

token=$(cat token.txt)
api=$(cat api.txt)

bl_countries=$(cat bl_countries.txt) # A list of blacklisted countries
wl_emails=$(cat wl_emails.txt) # A list of whitelisted email's domain names
bl_emails=$(cat bl_emails.txt) # A list of blacklisted email's domain names
wl_users=$(cat wl_users.txt) # A list of whitelisted user IDs

ids=$(curl --silent --request GET --header "PRIVATE-TOKEN: $token" "$api/users?order_by=id&sort=desc&per_page=200&without_projects=true" | jq '.[].id')

for id in $ids
do
	api_output=$(curl --silent --request GET --header "PRIVATE-TOKEN: $token" "$api/users/$id")

	username=$(echo $api_output | jq -r '.username')
	email=$(echo $api_output | jq -r '.email')
	bio=$(echo $api_output | jq -r '.bio')
	website_url=$(echo $api_output | jq -r '.website_url')
	created_at=$(echo $api_output | jq -r '.created_at')
	confirmed_at=$(echo $api_output | jq -r '.confirmed_at')
	last_activity_on=$(echo $api_output | jq -r '.last_activity_on')
	last_sign_in_at=$(echo $api_output | jq -r '.last_sign_in_at')
	memberships_count=$(( $(curl --silent --request GET --header "PRIVATE-TOKEN: $token" "$api/users/$id/memberships" | jq -r '. | length') ))
	is_admin=$(echo $api_output | jq -r '.is_admin')
	current_sign_in_ip=$(echo $api_output | jq -r '.current_sign_in_ip')
	country=$(whois $current_sign_in_ip | awk -F':[ \t]+' 'tolower($1) ~ /^country$/ { print toupper($2) }' | head -1)

	potential_spam="false"

	for bl_country in $bl_countries
	do
		if [ "$bl_country" == "$country" ]; then
			potential_spam="true"
		fi
	done

	seconds_to_confirm=-1

	if [ "$confirmed_at" == "null" ]; then
		confirmed_at="not confirmed"
		potential_spam="true"
	else
		seconds_to_confirm=$(( $(date -d "$confirmed_at" +"%s")-$(date -d "$created_at" +"%s") ))
		confirmed_at="confirmed at: $confirmed_at in $seconds_to_confirm seconds"
		if [ $(( $seconds_to_confirm )) -le 10 ]; then
			potential_spam="true"
		fi
	fi

	if [ "$last_sign_in_at" == "null" ]; then
		last_sign_in_at="never"
		potential_spam="true"
	fi

	if [ "$last_activity_on" == "null" ]; then
		last_activity_on="never"
	fi

	if [ $memberships_count -gt 0 ]; then
		potential_spam="false"
	fi

	for wl_email in $wl_emails
	do
		if [[ $email == *"$wl_email"* ]]; then
			potential_spam="false"
			email="$email (whitelisted)"
		fi
	done

	for wl_user in $wl_users
	do
		if [[ $id == "$wl_user" ]]; then
			potential_spam="false"
			username="$username (whitelisted)"
		fi
	done

	for bl_email in $bl_emails
	do
		if [[ $email == *"$bl_email"* ]]; then
			potential_spam="true"
			email="$email (blacklisted)"
		fi
	done

	if [ "$is_admin" == "true" ]; then
		potential_spam="false"
		is_admin="Yes"
	else
		is_admin="No"
	fi

	echo "($id) $username"
	echo "Email: $email"
	echo "Created at: $created_at ($confirmed_at)"
	echo "Bio: $bio"
	echo "Website: $website_url"
	echo "Last sign in: $last_sign_in_at"
	echo "Last activity: $last_activity_on"
	echo "Number of projects or groups: $memberships_count"
	echo "Sign in IP: $current_sign_in_ip ($country)"
	echo "Is admin: $is_admin"
	echo "Potential spam: $potential_spam"

	if [ "$potential_spam" == "true" ] && [ "$1" == "--delete" ];then
		curl --silent --request DELETE --header "PRIVATE-TOKEN: $token" "$api/users/$id?hard_delete=true"
		echo "DELETED!"
	fi

	if [ "$potential_spam" == "true" ] && [ "$1" == "--ask" ];then
		read -p "Delete? [n/(y)] " delete
		if [ "$delete" == "y" ] || [ "$delete" == "" ]; then
			curl --silent --request DELETE --header "PRIVATE-TOKEN: $token" "$api/users/$id?hard_delete=true"
			echo "DELETED!"
		fi
	fi

	echo ""
done