# THIS FILE LISTS SNIPPETS TITLES AND THEIR AUTHOR. USE THE --ask OPTION TO
# ASK TO DELETE THEIR AUTHOR.
#
# API ENDPOINT GOES TO api.txt: echo -n "https://my-gitlab-instance/api/v4" > api.txt
# TOKEN GOES TO token.txt: echo -n my_admin_token > token.txt

set -e

token=$(cat token.txt)
api=$(cat api.txt)

wl_emails=$(cat wl_emails.txt) # A list of whitelisted email's domain names
wl_users=$(cat wl_users.txt) # A list of whitelisted user IDs

ids=$(curl --silent --request GET --header "PRIVATE-TOKEN: $token" "$api/snippets/public?per_page=100" | jq '.[].id')

if [ "$(echo $ids | wc -l)" -eq 0 ]; then
	echo "Nothing to do."
	exit 1
fi

deleted_userids=""

manage_prompt () {
	read -p "Delete author? [(y)/n/d] " delete
	if [ "$delete" == "y" ] || [ "$delete" == "" ]; then
		curl --silent --request DELETE --header "PRIVATE-TOKEN: $token" "$api/users/$userid?hard_delete=true"
		echo "DELETED!"
		deleted_userids="$userid $deleted_userids"
	fi

	if [ "$delete" == "d" ]; then
		echo -n "Description: "
		echo "$api_output" | jq -r '.description'
		manage_prompt
	fi
}

for id in $ids
do
	api_output=$(curl --silent --request GET --header "PRIVATE-TOKEN: $token" "$api/snippets/$id")
	missing_snippet=$(echo $api_output | jq -r '.message')

	if [ "$missing_snippet" == "null" ]; then
		userid=$(echo $api_output | jq -r '.author.id')

		to_skip="false"
		for deleted_userid in $deleted_userids
		do
			if [ "$userid" == "$deleted_userid" ]; then
				to_skip="true"
			fi
		done

		if [ "$to_skip" == "false" ]; then
			title=$(echo $api_output | jq -r '.title')
			username=$(echo $api_output | jq -r '.author.username')
			api_output_user=$(curl --silent --request GET --header "PRIVATE-TOKEN: $token" "$api/users/$userid")
			whitelisted="false"

			for wl_user in $wl_users
			do
				if [[ $userid == "$wl_user" ]]; then
					whitelisted="true"
					userid="$userid, whitelisted"
				fi
			done

			useremail=$(echo $api_output_user | jq -r '.email')

			for wl_email in $wl_emails
			do
				if [[ $useremail == *"$wl_email"* ]]; then
					whitelisted="true"
					useremail="$useremail (whitelisted)"
				fi
			done

			echo "($id) $title"
			echo "User: $username ($userid)"
			echo "User email: $useremail"

			if [ "$whitelisted" != "true" ]; then
				if [ "$1" == "--ask" ]; then
					manage_prompt
				fi
			fi

			echo ""
		fi
	fi
done

bash $0 $1